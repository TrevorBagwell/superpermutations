from guppy import hpy
from timeit import default_timer as timer
_debug = False
_testing = True
symbols = "abcdefghijklmnopqrstuvwxyz"


def permutation(_str):
    if(len(_str) == 1):
        return [_str]
    _final = []
    _chars = _str
    _parts = [None]*len(_chars)
    for i in range(len(_chars)):
        if(i == 0):
            _parts[i] = _chars[1:]
        if(i < len(_chars)-1 and 0 < i):
            _parts[i] = _chars[:i]+_chars[i+1:]
        if(i == len(_chars)-1):
            _parts[i] = _chars[0:len(_chars)-1]
    for i in range(len(_chars)):
        _char = _chars[i]
        for _perm in permutation(_parts[i]):
            _final.append(_char+_perm)
    return _final



def n_n(n):
    _str = ""
    for m in range(n):
        _str = _str + symbols[m]
    return _str

def convertListToString(lst):
    _str = ""
    for l in lst:
        _str = _str + l
    return _str

# Memoized factorial algorithm
factorials = {0: 1, 1: 1, 2:2}
def factorial(n):
    if n not in factorials:
        factorials[n] = n * factorial(n-1)
    return factorials[n]

if _testing:
    print(factorial(5))

# Memoized factorial sum algorithm
factorialSums = {0:0, 1:1}
def factorialSum(n):
    if n not in factorialSums:
        factorialSums[n] = factorial(n) + factorialSum(n-1)
    return factorialSums[n]

if _testing:
    print(factorialSum(12))


def subsplits(_str, _len):
    lst = []
    for i in range(len(_str)-_len+1):
        lst.append(_str[i:i+_len])
    return lst

def isSuperpermute(_str):
    _symbs = "".join(sorted(set(_str)))
    print(_symbs)
    _perms = permutation(_symbs)
    _subs = set(subsplits(_str, len(_symbs)))
    if _debug: 
        print(_perms, _subs)
    flag = True
    for p in _perms:
        _p_in_subs = (p in _subs)
        flag =  _p_in_subs and flag
        # Print the bad value of if debugging
        if not _p_in_subs and _debug:
            print(p)
    return flag


if _testing: 
    print(isSuperpermute("123121321"))

# shortest possible merge where the outcome and the 
def bestMerge(a,b):
    largestK = 0
    _min = min(len(a),len(b))
    for k in range(_min):
        y = b[:k+1]
        x = a[len(a)-k-1:]
        if(x==y):
            largestK = k
    return a + b[largestK+1:len(b)]

def mushT(T):
    _T = []
    it = 0
    while(it < len(T)):
        t = T[it]
        it = it+1
        while(t[0] != t[-1] and it < len(T)):
            _t = T[it]
            it = it + 1
            t = bestMerge(t,_t)
        _T.append(t)
    return _T



superPermDict = {0:"",1:"a", 2:"aba"}
# n is an integer and returns the super permutation of "12...n"
def superPermute(n):
    if n not in superPermDict:
        _prev = superPermute(n-1)
        _prevSubs = subsplits(_prev, n-1)
        _prevChars = _prev[n-1:]
        # Create the T array
        T = []
        for i in range(len(_prevChars)):
            T.append(_prevSubs[i]+_prevChars[i])
        T.append(n_n(n-1)[::-1])
        _T = mushT(T)
        _T = [n_n(n-1)] + _T
        superPermDict[n] = symbols[n-1].join(_T)
    return superPermDict[n]
    



'''
_len = 10
_super = superPermute(_len)
isSuper = isSuperpermute(_super)
print(_len, isSuper, len(_super), factorialSum(_len))
'''

print(hpy().heap())

for i in range(12,13):
    print('\n\n\n')
    _super = superPermute(i)
    print(i, len(_super), factorialSum(i))
    print(isSuperpermute(_super))
    print(hpy().heap())



